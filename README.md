# Sakila Web (sakila-web)
This is a project that pretend to be an implementation for MySQL
[Sakila sample database](https://dev.mysql.com/doc/sakila/en/) in a Java EE
application. The Sakila sample database is designed to represent a DVD rental
store and Sakila Web is the front end as a web application.

## Installation
### Requirements
This project require:
* JDK 1.7
* WildFly or GlassFish as Java EE application server.
* MySQL Server as database engine.
* Maven

### Building from sources
This project is ready for compile using Maven.
- Compile with `mvn clean install`
- For to run, start your application server and deploy the war file that maven
create on target folder.

## License
Please refer to file [LICENSE](LICENSE.md), available in this repository.