/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 * This exception occurs when the entity has a relation with another one and the last one could
 * become orphaned.
 */
public class IllegalOrphanException extends Exception {

    /** Use serialVersionUID from JDK 1.0.2 for interoperability. */
    private static final long serialVersionUID = -7665223745681513586L;

    /** The detailed messages. */
    private final List<String> messages;

    /**
     * Constructs a new exception with the specified detailed messages.
     *
     * @param messages the detailed messages.
     */
    public IllegalOrphanException(final List<String> messages) {
        super((messages != null && messages.size() > 0
                ? messages.get(0) : null));

        if (messages == null) {
            this.messages = new ArrayList<>();
        } else {
            this.messages = messages;
        }
    }

    /**
     * Returns the detailed messages array of string of this exception.
     *
     * @return the detailed messages
     */
    public List<String> getMessages() {
        return messages;
    }
}
