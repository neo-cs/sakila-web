/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "address", catalog = "sakila", schema = "")
public class AddressEntity implements Serializable {

    private static final long serialVersionUID = 8049386928502158803L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "address_id")
    private Short addressId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "address")
    private String address;

    @Size(max = 50)
    @Column(name = "address2")
    private String address2;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "district")
    private String district;

    @Size(max = 10)
    @Column(name = "postal_code")
    private String postalCode;

    @Pattern(regexp = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", 
            message = "Invalid phone/fax format, should be as xxx-xxx-xxxx")
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "phone")
    private String phone;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @JoinColumn(name = "city_id", referencedColumnName = "city_id")
    @ManyToOne(optional = false)
    private CityEntity cityId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressId")
    private Collection<StaffEntity> staffEntityCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressId")
    private Collection<StoreEntity> storeEntityCollection;

    public AddressEntity() {
    }

    public AddressEntity(Short addressId) {
        this.addressId = addressId;
    }

    /**
     * Constructs a new Address entity with ID, address, district, phone and the last date of 
     * updating this data.
     * 
     * @param addressId the ID of the address.
     * @param address the address.
     * @param district the district.
     * @param phone the phone.
     * @param lastUpdate the last date of udpate.
     */
    public AddressEntity(Short addressId, String address, String district, String phone,
            Date lastUpdate) {
        this.addressId = addressId;
        this.address = address;
        this.district = district;
        this.phone = phone;
        this.lastUpdate = lastUpdate;
    }

    public Short getAddressId() {
        return addressId;
    }

    public void setAddressId(Short addressId) {
        this.addressId = addressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public CityEntity getCityId() {
        return cityId;
    }

    public void setCityId(CityEntity cityId) {
        this.cityId = cityId;
    }

    public Collection<StaffEntity> getStaffEntityCollection() {
        return staffEntityCollection;
    }

    public void setStaffEntityCollection(Collection<StaffEntity> staffEntityCollection) {
        this.staffEntityCollection = staffEntityCollection;
    }

    public Collection<StoreEntity> getStoreEntityCollection() {
        return storeEntityCollection;
    }

    public void setStoreEntityCollection(Collection<StoreEntity> storeEntityCollection) {
        this.storeEntityCollection = storeEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 56;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AddressEntity)) {
            return false;
        }
        
        AddressEntity other = (AddressEntity) object;
        
        return !((this.addressId == null && other.addressId != null) 
                || (this.addressId != null && !this.addressId.equals(other.addressId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.AddressEntity[ addressId=" + addressId + " ]";
    }

}
