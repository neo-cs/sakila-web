/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "category", catalog = "sakila", schema = "")
public class CategoryEntity implements Serializable {

    private static final long serialVersionUID = -7243509824164168030L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "category_id")
    private Short categoryId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "categoryEntity", fetch = FetchType.LAZY)
    private List<FilmCategoryEntity> filmCategoryEntityList;

    public CategoryEntity() {
    }

    public CategoryEntity(Short categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Constructs a new Category entity with ID, name and the last date of updating this data.
     *
     * @param categoryId the ID of the category
     * @param name the name of the category.
     * @param lastUpdate the last date of udpate.
     */
    public CategoryEntity(Short categoryId, String name, Date lastUpdate) {
        this.categoryId = categoryId;
        this.name = name;
        this.lastUpdate = lastUpdate;
    }

    public Short getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Short categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<FilmCategoryEntity> getFilmCategoryEntityList() {
        return filmCategoryEntityList;
    }

    public void setFilmCategoryEntityList(List<FilmCategoryEntity> filmCategoryEntityList) {
        this.filmCategoryEntityList = filmCategoryEntityList;
    }

    @Override
    public int hashCode() {
        int hash = 90;
        hash += (categoryId != null ? categoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof CategoryEntity)) {
            return false;
        }
        
        CategoryEntity other = (CategoryEntity) object;
        
        return !((this.categoryId == null && other.categoryId != null) 
                || (this.categoryId != null && !this.categoryId.equals(other.categoryId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.CategoryEntity[ categoryId=" + categoryId + " ]";
    }
    
}
