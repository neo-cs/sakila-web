/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "country", catalog = "sakila", schema = "")
public class CountryEntity implements Serializable {

    private static final long serialVersionUID = 5066114174065887576L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "country_id")
    private Short countryId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "country")
    private String country;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "countryId")
    private Collection<CityEntity> cityEntityCollection;

    public CountryEntity() {
    }

    public CountryEntity(Short countryId) {
        this.countryId = countryId;
    }

    /**
     * Constructs a new Country entity with ID, country name and the last date of updating this
     * data.
     * 
     * @param countryId the country ID.
     * @param country the country name.
     * @param lastUpdate the last date of update.
     */
    public CountryEntity(Short countryId, String country, Date lastUpdate) {
        this.countryId = countryId;
        this.country = country;
        this.lastUpdate = lastUpdate;
    }

    public Short getCountryId() {
        return countryId;
    }

    public void setCountryId(Short countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Collection<CityEntity> getCityEntityCollection() {
        return cityEntityCollection;
    }

    public void setCityEntityCollection(Collection<CityEntity> cityEntityCollection) {
        this.cityEntityCollection = cityEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 82;
        hash += (countryId != null ? countryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CountryEntity)) {
            return false;
        }

        CountryEntity other = (CountryEntity) object;

        return !((this.countryId == null && other.countryId != null) 
                || (this.countryId != null && !this.countryId.equals(other.countryId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.CountryEntity[ countryId=" + countryId + " ]";
    }

}
