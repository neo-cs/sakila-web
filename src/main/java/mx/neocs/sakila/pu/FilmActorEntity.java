/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "film_actor", catalog = "sakila", schema = "")
public class FilmActorEntity implements Serializable {

    private static final long serialVersionUID = 4493997023742607166L;
    
    @EmbeddedId
    protected FilmActorPrimaryKey filmActorPrimaryKey;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @JoinColumn(name = "actor_id", referencedColumnName = "actor_id", insertable = false, 
            updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ActorEntity actorEntity;

    @JoinColumn(name = "film_id", referencedColumnName = "film_id", insertable = false, 
            updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FilmEntity filmEntity;

    public FilmActorEntity() {
    }

    public FilmActorEntity(FilmActorPrimaryKey filmActorEntityPrimaryKey) {
        this.filmActorPrimaryKey = filmActorEntityPrimaryKey;
    }

    public FilmActorEntity(FilmActorPrimaryKey filmActorEntityPrimaryKey, Date lastUpdate) {
        this.filmActorPrimaryKey = filmActorEntityPrimaryKey;
        this.lastUpdate = lastUpdate;
    }

    public FilmActorEntity(short actorId, short filmId) {
        this.filmActorPrimaryKey = new FilmActorPrimaryKey(actorId, filmId);
    }

    public FilmActorPrimaryKey getFilmActorPrimaryKey() {
        return filmActorPrimaryKey;
    }

    public void setFilmActorPrimaryKey(FilmActorPrimaryKey filmActorPrimaryKey) {
        this.filmActorPrimaryKey = filmActorPrimaryKey;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public ActorEntity getActorEntity() {
        return actorEntity;
    }

    public void setActorEntity(ActorEntity actorEntity) {
        this.actorEntity = actorEntity;
    }

    public FilmEntity getFilmEntity() {
        return filmEntity;
    }

    public void setFilmEntity(FilmEntity filmEntity) {
        this.filmEntity = filmEntity;
    }

    @Override
    public int hashCode() {
        int hash = 78;
        hash += (filmActorPrimaryKey != null ? filmActorPrimaryKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof FilmActorEntity)) {
            return false;
        }
        
        FilmActorEntity other = (FilmActorEntity) object;
        
        return !((this.filmActorPrimaryKey == null && other.filmActorPrimaryKey != null) 
                || (this.filmActorPrimaryKey != null && !this.filmActorPrimaryKey
                        .equals(other.filmActorPrimaryKey)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.FilmActorEntity[ filmActorEntityPK=" + filmActorPrimaryKey + " ]";
    }
    
}
