/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Embeddable
public class FilmActorPrimaryKey implements Serializable {

    private static final long serialVersionUID = -2273911478484865206L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "actor_id")
    private short actorId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "film_id")
    private short filmId;

    public FilmActorPrimaryKey() {
    }

    public FilmActorPrimaryKey(short actorId, short filmId) {
        this.actorId = actorId;
        this.filmId = filmId;
    }

    public short getActorId() {
        return actorId;
    }

    public void setActorId(short actorId) {
        this.actorId = actorId;
    }

    public short getFilmId() {
        return filmId;
    }

    public void setFilmId(short filmId) {
        this.filmId = filmId;
    }

    @Override
    public int hashCode() {
        int hash = 23;
        hash += (int) actorId;
        hash += (int) filmId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof FilmActorPrimaryKey)) {
            return false;
        }

        FilmActorPrimaryKey other = (FilmActorPrimaryKey) object;

        if (this.actorId != other.actorId) {
            return false;
        }

        return this.filmId == other.filmId;
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.FilmActorEntityPK[ actorId=" + actorId + ", filmId=" + filmId 
                + " ]";
    }
    
}
