/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "film_category", catalog = "sakila", schema = "")
public class FilmCategoryEntity implements Serializable {

    private static final long serialVersionUID = -4561819842835946196L;

    @EmbeddedId
    protected FilmCategoryPrimaryKey filmCategoryPrimaryKey;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @JoinColumn(name = "category_id", referencedColumnName = "category_id", insertable = false,
            updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CategoryEntity categoryEntity;

    @JoinColumn(name = "film_id", referencedColumnName = "film_id", insertable = false,
            updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FilmEntity filmEntity;

    public FilmCategoryEntity() {
    }

    public FilmCategoryEntity(FilmCategoryPrimaryKey filmCategoryEntityPrimaryKey) {
        this.filmCategoryPrimaryKey = filmCategoryEntityPrimaryKey;
    }

    public FilmCategoryEntity(FilmCategoryPrimaryKey filmCategoryEntityPrimaryKey, 
            Date lastUpdate) {
        this.filmCategoryPrimaryKey = filmCategoryEntityPrimaryKey;
        this.lastUpdate = lastUpdate;
    }

    public FilmCategoryEntity(short filmId, short categoryId) {
        this.filmCategoryPrimaryKey = 
                new FilmCategoryPrimaryKey(filmId, categoryId);
    }

    public FilmCategoryPrimaryKey getFilmCategoryPrimaryKey() {
        return filmCategoryPrimaryKey;
    }

    public void setFilmCategoryPrimaryKey(
            FilmCategoryPrimaryKey filmCategoryPrimaryKey) {
        this.filmCategoryPrimaryKey = filmCategoryPrimaryKey;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    public FilmEntity getFilmEntity() {
        return filmEntity;
    }

    public void setFilmEntity(FilmEntity filmEntity) {
        this.filmEntity = filmEntity;
    }

    @Override
    public int hashCode() {
        int hash = 45;
        hash += (filmCategoryPrimaryKey != null ? filmCategoryPrimaryKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof FilmCategoryEntity)) {
            return false;
        }

        FilmCategoryEntity other = (FilmCategoryEntity) object;

        return !((this.filmCategoryPrimaryKey == null 
                && other.filmCategoryPrimaryKey != null) 
                || (this.filmCategoryPrimaryKey != null 
                && !this.filmCategoryPrimaryKey
                        .equals(other.filmCategoryPrimaryKey)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.FilmCategoryEntity[ filmCategoryEntityPK=" 
                + filmCategoryPrimaryKey + " ]";
    }
    
}
