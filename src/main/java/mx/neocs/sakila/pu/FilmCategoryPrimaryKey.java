/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Embeddable
public class FilmCategoryPrimaryKey implements Serializable {

    private static final long serialVersionUID = 7530042784924646128L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "film_id")
    private short filmId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "category_id")
    private short categoryId;

    public FilmCategoryPrimaryKey() {
    }

    public FilmCategoryPrimaryKey(short filmId, short categoryId) {
        this.filmId = filmId;
        this.categoryId = categoryId;
    }

    public short getFilmId() {
        return filmId;
    }

    public void setFilmId(short filmId) {
        this.filmId = filmId;
    }

    public short getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(short categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int hashCode() {
        int hash = 89;
        hash += (int) filmId;
        hash += (int) categoryId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof FilmCategoryPrimaryKey)) {
            return false;
        }
        
        FilmCategoryPrimaryKey other = (FilmCategoryPrimaryKey) object;
        
        if (this.filmId != other.filmId) {
            return false;
        }
        
        return this.categoryId == other.categoryId;
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.FilmCategoryEntityPK[ filmId=" + filmId + ", categoryId=" 
                + categoryId + " ]";
    }
    
}
