/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import mx.neocs.sakila.film.Rate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "film", catalog = "sakila", schema = "")
public class FilmEntity implements Serializable {

    private static final long serialVersionUID = 63436938397362577L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "film_id")
    private Short filmId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "title")
    private String title;

    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    @Column(name = "release_year")
    @Temporal(TemporalType.DATE)
    private Date releaseYear;

    @Basic(optional = false)
    @NotNull
    @Column(name = "rental_duration")
    private Short rentalDuration;

    @Basic(optional = false)
    @NotNull
    @Column(name = "rental_rate")
    private BigDecimal rentalRate;

    @Column(name = "runtime")
    private Short runtime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "replacement_cost")
    private BigDecimal replacementCost;

    @Enumerated(EnumType.STRING)
    @Column(name = "rating")
    private Rate rating;

    @Size(max = 54)
    @Column(name = "special_features")
    // SET('Trailers','Commentaries','Deleted Scenes','Behind the Scenes')
    private String specialFeatures;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filmEntity", fetch = FetchType.LAZY)
    private List<FilmCategoryEntity> filmCategories;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filmEntity", fetch = FetchType.LAZY)
    private List<FilmActorEntity> filmActors;

    @JoinColumn(name = "language_id", referencedColumnName = "language_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private LanguageEntity languageId;

    @JoinColumn(name = "original_language_id", referencedColumnName = "language_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private LanguageEntity originalLanguageId;

    public FilmEntity() {
    }

    public FilmEntity(Short filmId) {
        this.filmId = filmId;
    }

    /**
     * Constructs a new Film entity with ID, title, rental duration, rental rate, replacement cost
     * and the last date of updating this data.
     *
     * @param filmId the film ID.
     * @param title the title of the film.
     * @param rentalDuration the rental duration.
     * @param rentalRate the rental rate.
     * @param replacementCost the replacement cost.
     * @param lastUpdate the last date of udpate.
     */
    public FilmEntity(Short filmId, String title, short rentalDuration, BigDecimal rentalRate,
            BigDecimal replacementCost, Date lastUpdate) {
        this.filmId = filmId;
        this.title = title;
        this.rentalDuration = rentalDuration;
        this.rentalRate = rentalRate;
        this.replacementCost = replacementCost;
        this.lastUpdate = lastUpdate;
    }

    public Short getFilmId() {
        return filmId;
    }

    public void setFilmId(Short filmId) {
        this.filmId = filmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Date releaseYear) {
        this.releaseYear = releaseYear;
    }

    public short getRentalDuration() {
        return rentalDuration;
    }

    public void setRentalDuration(short rentalDuration) {
        this.rentalDuration = rentalDuration;
    }

    public BigDecimal getRentalRate() {
        return rentalRate;
    }

    public void setRentalRate(BigDecimal rentalRate) {
        this.rentalRate = rentalRate;
    }

    public Short getLength() {
        return runtime;
    }

    public void setLength(Short length) {
        this.runtime = length;
    }

    public BigDecimal getReplacementCost() {
        return replacementCost;
    }

    public void setReplacementCost(BigDecimal replacementCost) {
        this.replacementCost = replacementCost;
    }

    public Rate getRating() {
        return rating;
    }

    public void setRating(Rate rating) {
        this.rating = rating;
    }

    public String getSpecialFeatures() {
        return specialFeatures;
    }

    public void setSpecialFeatures(String specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<FilmCategoryEntity> getFilmCategories() {
        return filmCategories;
    }

    public void setFilmCategories(List<FilmCategoryEntity> filmCategories) {
        this.filmCategories = filmCategories;
    }

    public List<FilmActorEntity> getFilmActors() {
        return filmActors;
    }

    public void setFilmActors(List<FilmActorEntity> filmActors) {
        this.filmActors = filmActors;
    }

    public LanguageEntity getLanguageId() {
        return languageId;
    }

    public void setLanguageId(LanguageEntity languageId) {
        this.languageId = languageId;
    }

    public LanguageEntity getOriginalLanguageId() {
        return originalLanguageId;
    }

    public void setOriginalLanguageId(LanguageEntity originalLanguageId) {
        this.originalLanguageId = originalLanguageId;
    }

    @Override
    public int hashCode() {
        int hash = 50;
        hash += (filmId != null ? filmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!(object instanceof FilmEntity)) {
            return false;
        }

        FilmEntity other = (FilmEntity) object;

        return !((this.filmId == null && other.filmId != null)
                || (this.filmId != null && !this.filmId.equals(other.filmId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.FilmEntity[ filmId=" + filmId + " ]";
    }

}
