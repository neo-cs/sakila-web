/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import mx.neocs.sakila.exceptions.IllegalOrphanException;
import mx.neocs.sakila.exceptions.NonexistentEntityException;
import mx.neocs.sakila.exceptions.PreexistingEntityException;
import mx.neocs.sakila.exceptions.RollbackFailureException;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * This class implements all the methods from {@link  JpaController}.
 *
 * @param <T> the entity.
 * @param <K> the key.
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
public class GenericJpaController<T, K> implements Serializable , JpaController<T, K> {

    private static final long serialVersionUID = -7482120554929417701L;

    @PersistenceContext(name = "sakila-pu")
    protected EntityManager em;

    protected Class<T> entityClass;

    public GenericJpaController() {
        entityClass = (Class<T>)getParameterClass(getClass(), 0);
    }

    private static Class<?> getParameterClass(Class<?> clazz, int index) {
        return (Class<?>) (((ParameterizedType) clazz.getGenericSuperclass())
                .getActualTypeArguments()[index]);
    }

    @Override
    public void create(T entity) throws PreexistingEntityException, RollbackFailureException,
            Exception {
        em.persist(entity);
    }

    @Override
    public void edit(T entity) throws IllegalOrphanException, NonexistentEntityException,
            RollbackFailureException, Exception {
        em.merge(entity);
    }

    @Override
    public T findById(K id) {
        return em.find(entityClass, id);
    }

    @Override
    public List<T> findAll() {
        return findPaged(true, -1, -1);
    }

    @Override
    public List<T> findPaged(int maxResults, int firstResult) {
        return findPaged(false, maxResults, firstResult);
    }

    private List<T> findPaged(boolean all, int maxResults, int firstResult) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> rt = cq.from(entityClass);
        cq.select(rt);
        TypedQuery<T> query = em.createQuery(cq);

        if (!all) {
            query.setMaxResults(maxResults);
            query.setFirstResult(firstResult);
        }

        return query.getResultList();
    }

    @Override
    public int getCount() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<T> rt = cq.from(entityClass);
        cq.select(cb.count(rt));
        Query query = em.createQuery(cq);

        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public void delete(K id) throws IllegalOrphanException, NonexistentEntityException,
            RollbackFailureException, Exception {
        T entity = em.getReference(entityClass, id);

        if (entity != null) {
            em.remove(entity);
        } else {
            throw new NonexistentEntityException("The entity non exist.");
        }
    }
}
