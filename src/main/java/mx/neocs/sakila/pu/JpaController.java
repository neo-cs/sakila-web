/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import mx.neocs.sakila.exceptions.IllegalOrphanException;
import mx.neocs.sakila.exceptions.NonexistentEntityException;
import mx.neocs.sakila.exceptions.PreexistingEntityException;
import mx.neocs.sakila.exceptions.RollbackFailureException;

import java.util.List;

/**
 * This interface define basic methods of JPA controller.
 * 
 * @param <T> the entity.
 * @param <K> the key.
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
public interface JpaController<T, K> {

    void create(T entity) throws PreexistingEntityException, RollbackFailureException, Exception;

    void edit(T entity) throws IllegalOrphanException, NonexistentEntityException, 
            RollbackFailureException, Exception;

    void delete(K key) throws IllegalOrphanException, NonexistentEntityException, 
            RollbackFailureException, Exception;
    
    T findById(K key);

    List<T> findAll();

    List<T> findPaged(int maxResults, int firstResult);

    int getCount();

}
