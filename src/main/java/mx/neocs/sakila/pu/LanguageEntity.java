/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "language", catalog = "sakila", schema = "")
public class LanguageEntity implements Serializable {

    private static final long serialVersionUID = -8459024194267490377L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "language_id")
    private Short languageId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "languageId", fetch = FetchType.LAZY)
    private List<FilmEntity> lenguages;

    @OneToMany(mappedBy = "originalLanguageId", fetch = FetchType.LAZY)
    private List<FilmEntity> originaslLenguages;

    public LanguageEntity() {
    }

    public LanguageEntity(Short languageId) {
        this.languageId = languageId;
    }

    /**
     * Constructs a new Lenguage entity with ID, name and the last date of updating this data.
     * 
     * @param languageId the ID of the lenguage.
     * @param name the name of the lenguage.
     * @param lastUpdate the last date of udpate.
     */
    public LanguageEntity(Short languageId, String name, Date lastUpdate) {
        this.languageId = languageId;
        this.name = name;
        this.lastUpdate = lastUpdate;
    }

    public Short getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Short languageId) {
        this.languageId = languageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<FilmEntity> getFilmEntityList() {
        return lenguages;
    }

    public void setFilmEntityList(List<FilmEntity> filmEntityList) {
        this.lenguages = filmEntityList;
    }

    public List<FilmEntity> getFilmEntityList1() {
        return originaslLenguages;
    }

    public void setFilmEntityList1(List<FilmEntity> filmEntityList1) {
        this.originaslLenguages = filmEntityList1;
    }

    @Override
    public int hashCode() {
        int hash = 73;
        hash += (languageId != null ? languageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        
        if (!(object instanceof LanguageEntity)) {
            return false;
        }
        
        LanguageEntity other = (LanguageEntity) object;
        
        return !((this.languageId == null && other.languageId != null) 
                || (this.languageId != null && !this.languageId.equals(other.languageId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.LanguageEntity[ languageId=" + languageId + " ]";
    }
    
}
