/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "module", catalog = "sakila", schema = "")
public class ModuleEntity implements Serializable {

    private static final long serialVersionUID = -4789202336725849230L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "module_id")
    private Integer moduleId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "url")
    private String url;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "moduleEntity")
    private Collection<PermissionEntity> permissions;

    public ModuleEntity() {
    }

    public ModuleEntity(Integer moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * Constructs a new Module entity with ID, name and the URL of the module.
     *
     * @param moduleId the ID of the module.
     * @param name the name of the module.
     * @param url the URL of the module.
     */
    public ModuleEntity(Integer moduleId, String name, String url) {
        this.moduleId = moduleId;
        this.name = name;
        this.url = url;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Collection<PermissionEntity> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<PermissionEntity> permissions) {
        this.permissions = permissions;
    }

    @Override
    public int hashCode() {
        int hash = 18;
        hash += (moduleId != null ? moduleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ModuleEntity)) {
            return false;
        }
        
        ModuleEntity other = (ModuleEntity) object;
        
        return !((this.moduleId == null && other.moduleId != null) 
                || (this.moduleId != null && !this.moduleId.equals(other.moduleId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.ModuleEntity[ idModule=" + moduleId + " ]";
    }
    
}
