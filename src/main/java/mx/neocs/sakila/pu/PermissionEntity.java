/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "permission", catalog = "sakila", schema = "")
public class PermissionEntity implements Serializable {

    private static final long serialVersionUID = 1702572658001796358L;

    @EmbeddedId
    protected PermissionPrimaryKey permissionPrimaryKey;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;

    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @JoinTable(
            name = "role_has_permission",
            joinColumns = {
                @JoinColumn(name = "permission_id", referencedColumnName = "permission_id"),
                @JoinColumn(name = "module_id", referencedColumnName = "module_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "role_id", referencedColumnName = "role_id")
            }
        )
    @ManyToMany
    private Collection<RoleEntity> roleEntityCollection;

    @JoinColumn(name = "module_id", referencedColumnName = "module_id", insertable = false,
            updatable = false)
    @ManyToOne(optional = false)
    private ModuleEntity moduleEntity;

    public PermissionEntity() {
    }

    public PermissionEntity(PermissionPrimaryKey permissionEntityPrimaryKey) {
        this.permissionPrimaryKey = permissionEntityPrimaryKey;
    }

    public PermissionEntity(PermissionPrimaryKey permissionEntityPrimaryKey, String name) {
        this.permissionPrimaryKey = permissionEntityPrimaryKey;
        this.name = name;
    }

    public PermissionEntity(int idPermission, int moduleId) {
        this.permissionPrimaryKey = new PermissionPrimaryKey(idPermission, moduleId);
    }

    public PermissionPrimaryKey getPermissionPrimaryKey() {
        return permissionPrimaryKey;
    }

    public void setPermissionPrimaryKey(PermissionPrimaryKey permissionPrimaryKey) {
        this.permissionPrimaryKey = permissionPrimaryKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<RoleEntity> getRoleEntityCollection() {
        return roleEntityCollection;
    }

    public void setRoleEntityCollection(Collection<RoleEntity> roleEntityCollection) {
        this.roleEntityCollection = roleEntityCollection;
    }

    public ModuleEntity getModuleEntity() {
        return moduleEntity;
    }

    public void setModuleEntity(ModuleEntity moduleEntity) {
        this.moduleEntity = moduleEntity;
    }

    @Override
    public int hashCode() {
        int hash = 20;
        hash += (permissionPrimaryKey != null ? permissionPrimaryKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PermissionEntity)) {
            return false;
        }

        PermissionEntity other = (PermissionEntity) object;

        return !((this.permissionPrimaryKey == null && other.permissionPrimaryKey != null)
                || (this.permissionPrimaryKey != null && !this.permissionPrimaryKey
                        .equals(other.permissionPrimaryKey)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.PermissionEntity[ permissionEntityPK=" + permissionPrimaryKey
                + " ]";
    }

}
