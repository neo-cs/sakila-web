/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Embeddable
public class PermissionPrimaryKey implements Serializable {

    private static final long serialVersionUID = -8753126529111829560L;

    @Basic(optional = false)
    @Column(name = "permission_id")
    private int permissionId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "module_id")
    private int moduleId;

    public PermissionPrimaryKey() {
    }

    public PermissionPrimaryKey(int idPermission, int moduleId) {
        this.permissionId = idPermission;
        this.moduleId = moduleId;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    @Override
    public int hashCode() {
        int hash = 43;
        hash += (int) permissionId;
        hash += (int) moduleId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PermissionPrimaryKey)) {
            return false;
        }

        PermissionPrimaryKey other = (PermissionPrimaryKey) object;

        if (this.permissionId != other.permissionId) {
            return false;
        }

        return this.moduleId == other.moduleId;
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.PermissionEntityPK[ idPermission=" + permissionId 
                + ", moduleId=" + moduleId + " ]";
    }
    
}
