/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "session", catalog = "sakila", schema = "")
public class SessionEntity implements Serializable {

    private static final long serialVersionUID = 6732877824328363558L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "session_id")
    private Integer sessionId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "logging_date")
    @Temporal(TemporalType.DATE)
    private Date loggingDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "logging_time")
    @Temporal(TemporalType.TIME)
    private Date loggingTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "expiration_date")
    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "expiration_time")
    @Temporal(TemporalType.TIME)
    private Date expirationTime;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hashToken")
    private String hashToken;

    @JoinColumns({
        @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
        @JoinColumn(name = "staff_id", referencedColumnName = "staff_id")
    })
    @ManyToOne(optional = false)
    private UserEntity userId;

    public SessionEntity() {
    }

    public SessionEntity(Integer sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Constructs a new Country entity with ID, date and time of logging, date and time of
     * expiration session, if is an active session and finally the hash of the token of the cookie
     * of web browser of the client.
     *
     * @param sessionId the session ID.
     * @param loggingDate the logging date.
     * @param loggingTime the logging time.
     * @param expirationDate the expiration date of the session.
     * @param expirationTime the expiration time of the session.
     * @param active if is active the session.
     * @param hashToken the hash token of the cookie in the web browser of the client.
     */
    public SessionEntity(Integer sessionId, Date loggingDate, Date loggingTime, Date expirationDate,
            Date expirationTime, boolean active, String hashToken) {
        this.sessionId = sessionId;
        this.loggingDate = loggingDate;
        this.loggingTime = loggingTime;
        this.expirationDate = expirationDate;
        this.expirationTime = expirationTime;
        this.active = active;
        this.hashToken = hashToken;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public Date getLoggingDate() {
        return loggingDate;
    }

    public void setLoggingDate(Date loggingDate) {
        this.loggingDate = loggingDate;
    }

    public Date getLoggingTime() {
        return loggingTime;
    }

    public void setLoggingTime(Date loggingTime) {
        this.loggingTime = loggingTime;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getHashToken() {
        return hashToken;
    }

    public void setHashToken(String hashToken) {
        this.hashToken = hashToken;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessionId != null ? sessionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SessionEntity)) {
            return false;
        }

        SessionEntity other = (SessionEntity) object;

        return !((this.sessionId == null && other.sessionId != null)
                || (this.sessionId != null && !this.sessionId.equals(other.sessionId)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.SessionEntity[ idSession=" + sessionId + " ]";
    }

}
