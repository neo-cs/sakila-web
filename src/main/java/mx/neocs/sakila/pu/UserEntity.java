/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Entity
@Table(name = "user", catalog = "sakila", schema = "")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 71110920257923176L;

    @EmbeddedId
    protected UserPrimaryKey userPrimaryKey;

    @Size(max = 16)
    @Column(name = "username")
    private String username;

    @Size(max = 100)
    @Column(name = "hash_password")
    private String hashPassword;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    @ManyToMany(mappedBy = "users")
    private Collection<RoleEntity> roles;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<SessionEntity> sessions;

    @JoinColumn(name = "staff_id", referencedColumnName = "staff_id", insertable = false,
            updatable = false)
    @ManyToOne(optional = false)
    private StaffEntity staffEntity;

    public UserEntity() {
    }

    public UserEntity(UserPrimaryKey userEntityPrimaryKey) {
        this.userPrimaryKey = userEntityPrimaryKey;
    }

    public UserEntity(UserPrimaryKey userEntityPrimaryKey, boolean active) {
        this.userPrimaryKey = userEntityPrimaryKey;
        this.active = active;
    }

    public UserEntity(int idUser, short staffId) {
        this.userPrimaryKey = new UserPrimaryKey(idUser, staffId);
    }

    public UserPrimaryKey getUserPrimaryKey() {
        return userPrimaryKey;
    }

    public void setUserPrimaryKey(UserPrimaryKey userPrimaryKey) {
        this.userPrimaryKey = userPrimaryKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Collection<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Collection<RoleEntity> roles) {
        this.roles = roles;
    }

    public Collection<SessionEntity> getSessions() {
        return sessions;
    }

    public void setSessions(Collection<SessionEntity> sessions) {
        this.sessions = sessions;
    }

    public StaffEntity getStaffEntity() {
        return staffEntity;
    }

    public void setStaffEntity(StaffEntity staffEntity) {
        this.staffEntity = staffEntity;
    }

    @Override
    public int hashCode() {
        int hash = 62;
        hash += (userPrimaryKey != null ? userPrimaryKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof UserEntity)) {
            return false;
        }

        UserEntity other = (UserEntity) object;

        return !((this.userPrimaryKey == null && other.userPrimaryKey != null)
                || (this.userPrimaryKey != null
                && !this.userPrimaryKey.equals(other.userPrimaryKey)));
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.UserEntity[ userEntityPK=" + userPrimaryKey + " ]";
    }

}
