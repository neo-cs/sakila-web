/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mx.neocs.sakila.pu;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import mx.neocs.sakila.exceptions.NonexistentEntityException;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
public class UserJpaController extends GenericJpaController<UserEntity, UserPrimaryKey>{
    
    private static final long serialVersionUID = -5522778663661967197L;
    
    private static final String FIND_BY_USERNAME_AND_HASH_PASSWORD = 
            "select user"
            + " from UserEntity as user"
            + " where user.username = :username and user.hashPassword = :hashPassword";
    
    public UserEntity findByUsernameHashPassword(String username, String hashPassword) throws NonexistentEntityException{
        TypedQuery<UserEntity> query = em.createQuery(FIND_BY_USERNAME_AND_HASH_PASSWORD, entityClass);
        query.setParameter("username", username);
        query.setParameter("hashPassword", hashPassword);

        try {
            return query.getSingleResult();
        } catch (NoResultException n){
            throw new NonexistentEntityException("No exist user: " + username, n);
        }
    }
}
