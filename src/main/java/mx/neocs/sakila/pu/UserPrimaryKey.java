/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.sakila.pu;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * This class represents the primary key of {@link UserEntity}.
 * 
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
@Embeddable
public class UserPrimaryKey implements Serializable {

    private static final long serialVersionUID = -3546454776049201191L;

    @Basic(optional = false)
    @Column(name = "user_id")
    private int userId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "staff_id")
    private short staffId;

    public UserPrimaryKey() {
    }

    public UserPrimaryKey(int userId, short staffId) {
        this.userId = userId;
        this.staffId = staffId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public short getStaffId() {
        return staffId;
    }

    public void setStaffId(short staffId) {
        this.staffId = staffId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) staffId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof UserPrimaryKey)) {
            return false;
        }

        UserPrimaryKey other = (UserPrimaryKey) object;

        if (this.userId != other.userId) {
            return false;
        }

        return this.staffId == other.staffId;
    }

    @Override
    public String toString() {
        return "mx.org.neocs.pu.UserEntityPK[ idUser=" + userId + ", staffId=" + staffId + " ]";
    }
    
}
