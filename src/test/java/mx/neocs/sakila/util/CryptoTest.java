/*
 * Copyright (C) 2016 Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mx.neocs.sakila.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Freddy Barrera [freddy.barrera.moo(at)gmail.com.mx]
 */
public class CryptoTest {

    private static final Logger LOGGER = Logger.getLogger(CryptoTest.class.getName());

    public CryptoTest() {
    }

    @Test
    public void hmacMessageTest() {
        String expect = "Lwked3YHQKIyhfUhxufhfJoDYYxL7t29oGiM0lPgZOuJjYtmZ6CiI45ACJOx6d+Je/ViLZ2darAd8nQgdAWeLA==";
        String result = Crypto.hmac("hello-world");
        LOGGER.log(Level.INFO, "Hash encoded (result): {0}", result);
        LOGGER.log(Level.INFO, "Hash encoded (expect): {0}", expect);

        assertTrue(expect.equals(result));
    }

    @Test
    public void hmacKeyMessageTest() {
        String expect = "3pN+/mUSXl63uuSfz8VGTHreUr5ax4upLuSnfa6flNXErCYQXlSP1kogjfh6xixcDV78AjX0l690vbAkkz/NiQ==";
        String result = Crypto.hmac("Life is good", "hello world");
        LOGGER.log(Level.INFO, "Hash encoded (result): {0}", result);
        LOGGER.log(Level.INFO, "Hash encoded (expect): {0}", expect);

        assertTrue(expect.equals(result));
    }
}
